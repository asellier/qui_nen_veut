<?php
include_once('header.php');
?>

<html>
    <head>
        <title>Enchere</title>
        <style>
            .col-center-block {
                float: none;
                display: block;
                margin-left: auto;
                margin-right: auto;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row myCenter">
                <div class="col-xs-6 col-md-4 col-center-block">
                    <form class="form-signin" method="post" action="../inscription.php">
                        <div class="row">
                            <div class="center-block">
                                <h1 class="red-text text-center"> Inscription <br/><small class="red-text text-center">Merci de renseigner vos informations</small> </h1>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="nom" class="sr-only">Nom</label>
                            <input type="text" id="nom" name="nom" class="form-control" placeholder="Nom" required autofocus>
                        </div>

                        <div class="form-group">
                            <label for="prenom" class="sr-only">Prénom</label>
                            <input type="text" id="prenom" name="prenom" class="form-control" placeholder="Prénom" required>
                        </div>

                        <div class="form-group">
                            <label for="email" class="sr-only">Adresse mail</label>
                            <input type="text" id="email" name="email" class="form-control" placeholder="Mail" required>
                        </div>

                        <div class="form-group">
                            <label for="password" class="sr-only">Mot de passe</label>
                            <input type="password" id="password" name="password" class="form-control" placeholder="Mot de passe" required>
                        </div>

                        <div class="form-group">
                            <label for="passconf" class="sr-only">Vérification mot de passe</label>
                            <input type="password" id="passconf" name="passconf" class="form-control" placeholder="Vérification mot de passe" required>
                        </div>

                        <div class="input-group">
                            <label for="adresse" class="sr-only">Adresse</label>
                            <span class="input-group-addon glyphicon glyphicon-globe"></span>
                            <input type="text" id="adresse" name="adresse" class="form-control" placeholder="Adresse" required>
                        </div>

                        <br>

                        <div class="form-group">
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Confirmer</button>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </body>
</html>

<?php
include_once('footer.php');
?>
