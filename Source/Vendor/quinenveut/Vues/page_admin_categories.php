<!DOCTYPE html>
<html>
    <head>
        <script>
            function del() {
                var msg = "Vérification de suppression";
                if (confirm(msg)==true){
                    return true;
                }else{
                    return false;
                }
            }
        </script>
    </head>
    <body>
    <?php
    include_once('header.php');
    ?>



    <div class="row">
        <section class="col-sm-8 col-md-offset-1 margin">
            <table class="table table-bordered table-striped table-condensed">
                <caption>
                    <h4>Liste des catégories</h4>
                </caption>
                <thead>
                <tr>
                    <th>Id</th>
                    <th>Description</th>
                    <th></th>
                    <th></th>
                </tr>
                </thead>
                <tbody>
                <?php
                require_once('../Categories.php');
                $categories_controller = new \quinenveut\Categories();
                $liste_categories = $categories_controller->list_all_categories_admin();

                while ($categorie = $liste_categories->fetch())
                {
                    echo '<tr><td>'.$categorie['id_cat'].'</td><td> '.$categorie['nom_cat'].'</td>
                    <td><form method="post" action="formulaire_modif_categories.php"><input type="hidden" name="nom_cat" value="'.$categorie['nom_cat'].' ">
                   <input type="hidden" name="id_cat" value="'.$categorie['id_cat'].' ">
                   <input class ="btn btn-warning" type="submit" name="submit" value="modifier"></form></td>
                   <td><form method="post" action="../suppr_categorie.php"><input type="hidden" name="id_cat" value="'.$categorie['id_cat'].' "><input class ="btn btn-danger" type="submit" value="Supprimer"  onclick="javascript:return del();"></form></td></td></tr>';
                }
                $liste_categories->closeCursor();
                ?>
                </tbody>
            </table>
            <a href="formulaire_ajout_categorie.php"><button class="btn btn-primary">ajouter</button></a> <br><br>
            <a href="page_admin.php"><button class="btn btn-primary">Retour</button></a>
        </section>
    </div>



    <?php
    include_once('footer.php');
    ?>
    </body>
</html>
