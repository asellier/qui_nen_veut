
<?php

    $categorie=$_GET['id_cat'];
    require_once('../Categories.php');
    $categorie_controller = new \quinenveut\Categories();
    $infos_categorie = $categorie_controller->get_categorie($categorie);
    $categories = $infos_categorie->fetch();


    ?>

<div class="row">
    <section class="col-sm-8 col-md-offset-1 margin">
        <table class="table table-bordered table-striped table-condensed">
            <caption>
                <h4>Liste des objets en vente pour La catégorie : <?php  echo $categories['nom_cat'] ?></h4>
            </caption>
            <thead>
            <tr>
                <th>Objet</th>
                <th>Description</th>
            </tr>
            </thead>
            <tbody>
            <?php
            require_once('../Objets.php');
            $objets_controller = new \quinenveut\Objets();
            $objets_controller->liste_objets_by_categorie($categorie);
            ?>
            </tbody>
        </table>
    </section>
</div>
