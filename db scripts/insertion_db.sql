USE m2test7;

INSERT INTO `Roles` (`id_role`, `nom_role`) VALUES ('1', 'Administrateur');
INSERT INTO `Roles` (`id_role`, `nom_role`) VALUES ('2', 'Utilisateur');
INSERT INTO `Roles` (`id_role`, `nom_role`) VALUES ('3', 'Visiteur');

INSERT INTO `Users` (`id_user`, `id_role`, `mail`, `mdp`, `nom`, `prenom`, `statut`, `adresse`) VALUES (NULL, '1', 'admin@admin.admin', 'admin', 'admin', NULL, '1', NULL);
INSERT INTO `Users` (`id_user`, `id_role`, `mail`, `mdp`, `nom`, `prenom`, `statut`, `adresse`) VALUES (NULL, '2', 'user@user.user', 'user', 'user', NULL, '1', NULL);
INSERT INTO `Users` (`id_user`, `id_role`, `mail`, `mdp`, `nom`, `prenom`, `statut`, `adresse`) VALUES (NULL, '3', 'visiteur@visiteur.visiteur', 'visiteur', 'visiteur', NULL, '1', NULL);
INSERT INTO `Users` (`id_user`, `id_role`, `mail`, `mdp`, `nom`, `prenom`, `statut`, `adresse`) VALUES (NULL, '2', 'user2@user.user', 'user2', 'user2', NULL, '1', NULL);
INSERT INTO `Users` (`id_user`, `id_role`, `mail`, `mdp`, `nom`, `prenom`, `statut`, `adresse`) VALUES (NULL, '2', 'user3@user.user', 'user3', 'user3', NULL, '0', NULL);

INSERT INTO `Visibilite` (`id_visibilite`, `nom_visibilite`) VALUES ('1', 'Publique');
INSERT INTO `Visibilite` (`id_visibilite`, `nom_visibilite`) VALUES ('2', 'Privee');
INSERT INTO `Visibilite` (`id_visibilite`, `nom_visibilite`) VALUES ('3', 'Confidentielle');

INSERT INTO `Categories` (`id_cat`, `nom_cat`) VALUES ('1', 'Indefini');
INSERT INTO `Categories` (`id_cat`, `nom_cat`) VALUES ('2', 'Jouets');
INSERT INTO `Categories` (`id_cat`, `nom_cat`) VALUES ('3', 'Vetements');

INSERT INTO `Objets` (`nom_objet`, `desc_objet`, `prix_base`, `prix_res`, `date_limit`, `heure_limit`, `id_cat`, `photo`, `id_vendeur`, `statut`, `id_visibilite`)
VALUES ('truc', 'joli truc', '1', '2', '2018-11-11', '14:30', '1', NULL, '2', '0', '1');
INSERT INTO `Objets` (`nom_objet`, `desc_objet`, `prix_base`, `prix_res`, `date_limit`, `heure_limit`, `id_cat`, `photo`, `id_vendeur`, `statut`, `id_visibilite`)
VALUES ('bidule', 'bidule chouette', '1', '2', '2018-12-12', '14:30', '2', NULL, '2','0', '1');
INSERT INTO `Objets` (`nom_objet`, `desc_objet`, `prix_base`, `prix_res`, `date_limit`, `heure_limit`, `id_cat`, `photo`, `id_vendeur`, `statut`, `id_visibilite`)
VALUES ('machin', 'machin chose', '1', '2', '2018-01-01', '14:30', '2', NULL, '2', '0', '1');
INSERT INTO `Objets` (`nom_objet`, `desc_objet`, `prix_base`, `prix_res`, `date_limit`, `heure_limit`, `id_cat`, `photo`, `id_vendeur`, `statut`, `id_visibilite`)
VALUES ('chose', 'bleue', '1', '2', '2018-01-01', '14:30', '3', NULL, '2', '0', '1');
INSERT INTO `Objets` (`nom_objet`, `desc_objet`, `prix_base`, `prix_res`, `date_limit`, `heure_limit`, `id_cat`, `photo`, `id_vendeur`, `statut`, `id_visibilite`)
VALUES ('chose', 'verte', '1', '2', '2018-01-01','14:30', '3', NULL, '2', '0', '1');

INSERT INTO `Encheres` (`Prix`, `id_objet`, `id_acheteur`) VALUES ('1', '1', '1');
INSERT INTO `Encheres` (`Prix`, `id_objet`, `id_acheteur`) VALUES ('2', '1', '3');
INSERT INTO `Encheres` (`Prix`, `id_objet`, `id_acheteur`) VALUES ('3', '1', '1');
INSERT INTO `Encheres` (`Prix`, `id_objet`, `id_acheteur`) VALUES ('4', '1', '3');
INSERT INTO `Encheres` (`Prix`, `id_objet`, `id_acheteur`) VALUES ('5', '1', '1');


INSERT INTO `Encheres` (`Prix`, `id_objet`, `id_acheteur`) VALUES ('1', '2', '1');
INSERT INTO `Encheres` (`Prix`, `id_objet`, `id_acheteur`) VALUES ('1', '3', '1');
INSERT INTO `Encheres` (`Prix`, `id_objet`, `id_acheteur`) VALUES ('1', '4', '1');
INSERT INTO `Encheres` (`Prix`, `id_objet`, `id_acheteur`) VALUES ('1', '5', '1');