<?php
require('../Users.php');
$users_controller = new \quinenveut\Users();
$infos_utilisateur = $users_controller->get_user($_GET['id_user']);
$utilisateur = $infos_utilisateur->fetch()
?>

<html>
    <head>
        <title>Profil</title>
    </head>
    <body>
        <br><br><br><br><br><br>
        <div class="col-md-12 col-md-offset-4 margin">
            <ul class="list-group col-sm-3" >
                <li class="list-group-item active"> Informations personnelles</li>
                <li class="list-group-item"><b>Nom :</b>     <?php echo $utilisateur['nom'] ?></li>
                <li class="list-group-item"><b>Prénom :</b>  <?php echo $utilisateur['prenom'] ?></li>
                <li class="list-group-item"><b>Mail :</b>    <?php echo $utilisateur['mail'] ?></li>
                <li class="list-group-item"><b>Adresse :</b> <?php echo $utilisateur['adresse'] ?></li>
            </ul>
        </div>
    </body>
</html>
