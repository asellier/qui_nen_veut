    <li><a href="page_admin_categories.php" id="admin_categories"><span class="glyphicon glyphicon-list"></span> Catégories</a></li>
    <li><a href="page_admin_objets.php" id="admin_objets"><span class="glyphicon glyphicon-piggy-bank"></span> Créer une enchère</a></li>
    <li><a href="page_admin_users.php" id="admin_users"><span class="glyphicon glyphicon-user"></span> Utilisateurs</a></li>
    <li><a href="page_admin.php" id="admin_recap"><span class="glyphicon glyphicon-edit"></span> Administration</a></li>
</ul>
<ul class="nav navbar-nav navbar-right">
    <li><a href="../deconnexion.php" id="deco"><span class="glyphicon glyphicon-log-out"></span> Se déconnecter</a></li>
