<?php

include_once('header.php');
include_once('details_objet.php');

if (!isset($_SESSION['id_user']))
{
    echo '<a href="choix_visiteur.php?id_objet=' . $_GET['id_objet'] . '"><button type="button" class="btn btn-success">Enchérir</button></a>';

}
else
{
    if ($_SESSION['role'] == 1)
    {
        echo '<a href="page_admin_objets.php"><button type="button" class="btn btn-warning">Gestion des enchères</button></a>';
    }
    else {
        if ($objet['id_vendeur'] <> $_SESSION['id_user'])
        {
            echo '<a href="formulaire_enchere.php?id_objet=' . $_GET['id_objet'] . '"><button type="button" class="btn btn-success">Enchérir</button></a>';
        }
        else
        {
            //TODO : editer une annonce
            //echo '<button type="button" class="btn btn-primary">Editer mon annonce</button>';
            echo '<button type="button" class="btn btn-primary">Vous ne pouvez pas enchérir sur vos objets</button>';
        }
    }
}

include_once('footer.php');

?>
