<?php

namespace quinenveut;

use PhpParser\Node\Scalar;

class Categories
{
    public function get_categorie($id_cat)
    {
        require('database.php');
        $liste_categories = $bdd->prepare('SELECT * FROM Categories WHERE id_cat = ? ;');
        $liste_categories->execute(array($id_cat)) or die (print_r($liste_categories->errorInfo()));
        return $liste_categories;
    }

    public function get_categories()
    {
        require('database.php');
        $liste_categories = $bdd->query('SELECT * FROM Categories;');
        return $liste_categories;
    }

    public function update_cat(){
        require('database.php');

        $nom_cat = $_POST['nom'];
        session_start();
        $id_cat = $_SESSION['id_cat'];
        $res = $bdd->query("UPDATE Categories SET nom_cat='$nom_cat' WHERE id_cat='$id_cat'");
        return $res;
    }

    public function delete_cat(){
        require('database.php');

        $id_cat = $_SESSION['id_cat'];
        $liste_objets = $bdd->prepare('SELECT * FROM Objets WHERE id_cat = ? ;');
        $liste_objets->execute(array($id_cat));


        while ($objet = $liste_objets->fetch()) {
            $id_obj = $objet['id_objet'];
            $bdd->query("DELETE FROM Encheres WHERE id_objet='$id_obj'");
        }

        $res = $bdd->query("DELETE FROM Categories WHERE id_cat='$id_cat'");

        return $res;
    }

    public function insert_cat(){
        require('database.php');

        $nom_cat = $_POST['nom'];
        $res = $bdd->query("INSERT INTO Categories(nom_cat) VALUES( '$nom_cat')");
        return $res;
    }

    public function list_all_categories()
    {
        $liste_categories = Categories::get_categories();
        while ($categorie = $liste_categories->fetch())
        {
            echo '<tr><td>'.$categorie['id_cat'].'</td><td> '.$categorie['nom_cat'].'</td><td><a href="page_objet_categorie.php?id_cat='.$categorie["id_cat"].'">Afficher</a></td></tr>';
        }
        $liste_categories->closeCursor();
    }


    public function list_all_categories_admin()
    {
        $liste_categories = Categories::get_categories();
        return $liste_categories;
    }

    public function modif_categorie(){
        if($this->update_cat() != false){
            $add = 'Vues/page_admin_categories.php';
            header("Location: $add");
            return $add;
        }else{
            $add = 'Vues/formulaire_modif_categories.php';
            header("Location: $add");
            return $add;
        }
    }

    public function suppr_categorie(){
        if($this->delete_cat()!= false){
            $add = 'Vues/page_admin_categories.php';
            echo "<script>alert('Supprimé avec succès!');location.href='$add'</script>";
            return 1;
        } else {
            $add = 'Vues/page_admin_categories.php';
            echo "<script>alert('Error!');location.href='$add'</script>";
            return 0;
        }
    }

    public function ajout_categorie(){
        if($this->insert_cat() != false){
            $add = 'Vues/page_admin_categories.php';
            echo "<script>alert('Ajouté avec succès!');location.href='$add'</script>";
            return $add;
        } else{
            $add = 'Vues/formulaire_ajout_categorie.php';
            echo "<script>alert('Error!');location.href='$add'</script>";
            return $add;
        }
    }
}
?>
