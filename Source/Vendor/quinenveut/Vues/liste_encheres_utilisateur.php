<div class="row">
    <section class="col-sm-8 col-md-offset-1 margin">
        <table class="table table-bordered table-striped table-condensed">
            <caption>
                <h4>Liste des enchères proposées</h4>
            </caption>
            <thead>
            <tr>
                <th>Objet</th>
                <th>Prix</th>
                <th>récapitulatif de l'enchère</th>
            </tr>
            </thead>
            <tbody>
<?php
require_once('../Encheres.php');
$encheres_controller = new \quinenveut\Encheres();
$encheres_controller->list_encheres_by_user($_SESSION['id_user']);
?>
            </tbody>
        </table>
    </section>
</div>
