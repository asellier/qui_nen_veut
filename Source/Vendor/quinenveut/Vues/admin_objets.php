<div class="row">
    <section class="col-sm-8 col-md-offset-1 margin">
        <table class="table table-bordered table-striped table-condensed">
            <caption>
                <h4>Liste des objets en vente</h4>
            </caption>
            <thead>
            <tr>
                <th>Objet</th>
                <th>Vendeur</th>
                <th>Meilleure enchère</th>
                <th>Fin de l'enchère</th>
                <th>Statut</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            require_once ('../Objets.php');
            $controller_objets = new \quinenveut\Objets();
            $infos_objets = $controller_objets->admin_all_objets();
            ?>
            </tbody>
        </table>
    </section>
</div>
<div class="row">
    <a href="page_validation_objets.php"><button class="btn btn-warning">Liste des enchères à valider</button></a><br><br>
    <a href="page_admin.php"><button class="btn btn-warning">Retour</button>
</div>