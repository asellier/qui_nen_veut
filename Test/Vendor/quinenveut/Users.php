<?php
namespace quinenveut\tests\units;

use \atoum;


class Users extends atoum
{
    // l'utilisateur existe dans la base de donnees
    public function testGetUser (){
        $this
            ->given($user = new \quinenveut\Users())
            ->and($id= 1)
            ->then
            // tester que la valeur retournee est un objet PDOStatement apres avoir utilise prepare() et execute()
            ->object($utilisateur=$user->get_user($id))
            // tester que on obtient l'utilisateur dont id_user est ce que on demande
            ->string($utilisateur->fetch()['id_user'])
            ->isIdenticalTo('1')  //$id
        ;
    }

    // l'utilisateur n'existe pas dans la base de donnnees
    public function testGetUserError (){
        $this
            ->given($user = new \quinenveut\Users())
            ->and($id_user = 200)   // l'utilisateur n'existe pas
            ->then
            ->object($utilisateur=$user->get_user($id_user))
            ->boolean($utilisateur->fetch())
            // la fonction ne trouve pas l'utilisateur
            ->isIdenticalTo(false)
        ;
    }

    // l'utilisateur existe dans la base de donnees
    public function testGetUserParMail(){
        $this
            ->given($user = new \quinenveut\Users())
            ->and($mail= "user@user.user")   // l'utilisateur dans la base de donnees
            ->then
            ->object($utilisateur=$user->get_user_by_mail($mail))
            ->string($utilisateur->fetch()['mail'])
            ->isIdenticalTo($mail)
        ;
    }

    // l'utilisateur n'existe pas dans la base de donnnees
    public function testGetUserParMailError (){
        $this
            ->given($user = new \quinenveut\Users())
            ->and($mail= "mailexistepas@gmail.com")
            ->then
            ->object($utilisateur=$user->get_user_by_mail($mail))
            ->boolean($utilisateur->fetch())
            ->isIdenticalTo(false)
        ;
    }

    //si l'utilisateur se connecte avec succes, la page tourne vers page accueil
    public function testConnexSucces(){
        $this
            ->given($user = new \Mock\quinenveut\Users())
            ->and($res = 1)

            //mock
            ->and($this->calling($user)->check = $res)

            ->then
            ->string($user->connecter())
            ->isIdenticalTo('Vues/page_accueil.php')
        ;
    }

    //si l'utilisateur ne se connecte pas avec succes, la page de connexion refresh
    public function testConnexError(){
        $this
            ->given($user = new \Mock\quinenveut\Users())
            ->and($res = 0)

            ->and($this->calling($user)->check = $res)

            ->then
            ->string($user->connecter())
            ->isIdenticalTo('Vues/formulaire_connexion.php')
        ;
    }

    //si l'utilisateur s'inscrit avec succes, la page tourne vers page accueil
    public function testInscription(){
        $this
            ->given($user = new \Mock\quinenveut\Users())
            ->and($res = 1)

            ->and($this->calling($user)->check2 = $res)

            ->then
            ->string($user->inscription())
            ->isIdenticalTo('Vues/page_accueil.php')
        ;
    }

    //si l'utilisateur ne s'inscrit pas avec succes(les 2 passwords ne sont pas memes), la page d'inscription refresh
    public function testInscriptionError1(){
        $this
            ->given($user = new \Mock\quinenveut\Users())
            ->and($res = 0)

            ->and($this->calling($user)->check2 = $res)

            ->then
            ->string($user->inscription())
            ->isIdenticalTo('Vues/formulaire_inscription.php')
        ;
    }

    //si l'utilisateur ne s'inscrit pas avec succes(email existe deja), la page d'inscription refresh
    public function testInscriptionError2(){
        $this
            ->given($user = new \Mock\quinenveut\Users())
            ->and($res = -1)

            ->and($this->calling($user)->check2 = $res)

            ->then
            ->string($user->inscription())
            ->isIdenticalTo('Vues/formulaire_inscription.php')
        ;
    }

}
