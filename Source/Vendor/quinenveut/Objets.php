<?php

namespace quinenveut;

class Objets
{
    public function __construct()
    {
        require_once('database.php');
    }

    public function get_objet($id_objet)
    {
        require('database.php');
        $liste_objets = $bdd->prepare('SELECT * FROM Objets WHERE id_objet = ? ;');
        $liste_objets->execute(array($id_objet)) or die (print_r($liste_objets->errorInfo()));
        return $liste_objets;
    }

    public function get_objets()
    {
        require('database.php');
        $liste_objets = $bdd->query('SELECT * FROM Objets;') or die (print_r($liste_objets->errorInfo()));
        return $liste_objets;
    }
    public function get_objets_by_categorie($id_categorie){

        require('database.php');
        $liste_objets = $bdd->prepare('SELECT * FROM Objets WHERE id_cat = ? ;');
        $liste_objets->execute(array($id_categorie)) or die (print_r($liste_objets->errorInfo()));
        return $liste_objets;

    }

    public function liste_objets_by_categorie($id_categorie) {
        require('Encheres.php');
        $liste_objets = Objets::get_objets_by_categorie($id_categorie);
        $objet = $liste_objets->fetch();
        while ($objet = $liste_objets->fetch()) {
            echo '<tr><td><a href="page_objet.php?id_objet=' . $objet['id_objet'] . '">' . $objet["nom_objet"] . '</td><td>' . $objet["desc_objet"] . '</td></tr>';
        }
        $liste_objets->closeCursor();

    }

    public function get_objets_by_user($id_user)
    {
        require('database.php');
        $liste_objets = $bdd->prepare('SELECT * FROM Objets WHERE id_vendeur = ? ;');
        $liste_objets->execute(array($id_user)) or die (print_r($liste_objets->errorInfo()));
        return $liste_objets;
    }

    public function admin_all_objets()
    {
        include_once('Users.php');
        $users_controller = new \quinenveut\Users();

        include_once('Encheres.php');
        $encheres_controller = new Encheres();

        $objets = new Objets();
        $liste_objets = $objets->get_objets();
        while ($objet = $liste_objets->fetch())
        {
            if ($objet['statut'] <> 0)
            {
                echo '<tr>';
                echo '<td><a href="page_objet.php?id_objet=' . $objet['id_objet'] . '">' . $objet['nom_objet'] . '</a></td>';
                $vendeur_infos = $users_controller->get_user($objet['id_vendeur']);
                $vendeur = $vendeur_infos->fetch();
                echo '<td> ' . $vendeur['nom'] . '</td>';
                $encheres_infos = $encheres_controller->get_enchere_max($objet['id_objet']);
                $enchere_info = $encheres_infos->fetch();
                echo '<td> ' . $enchere_info['prix'] . ' €</td>';
                echo '<td> ' . $objet['date_limit'] . '</td>';
                if ($objet['statut'] == 1)
                {
                    echo '<td>En cours</td>';
                }
                if ($objet['statut'] == 2)
                {
                    echo '<td>Terminée</td>';
                }
                if ($objet['statut'] == 3)
                {
                    echo '<td>Annulée</td>';
                }
                echo '<td><a href="page_objet.php?id_objet=' . $objet['id_objet'] . '"><button class="btn btn-primary">Détails</button></a></td>';
                echo '<td><a href="../supprimer_objet.php?id_objet=' . $objet['id_objet'].'"onclick="return confirm(\'Vérification de suppression?\');"><button class="btn btn-danger">Supprimer</button></a></td>';
                echo '</tr>';
            }
        }
        $liste_objets->closeCursor();
    }

    public function list_all_objets()
    {
        include_once('Encheres.php');
        $objets = new Objets();
        $liste_objets = $objets->get_objets();
        while ($objet = $liste_objets->fetch())
        {
            if(self::vendu($objet['id_objet'])<2) {
                if ($objet['statut'] == 1)
                {
                    echo '<tr>';
                    echo '<td><a href="page_objet.php?id_objet=' . $objet['id_objet'] . '">' . $objet['nom_objet'] . '</a></td>';
                    echo '<td>' . $objet['desc_objet'] . '</td>';
                    $encheres_controller = new Encheres();
                    $encheres_infos = $encheres_controller->get_enchere_max($objet['id_objet']);
                    $enchere_info = $encheres_infos->fetch();
                    echo '<td> ' . $enchere_info['prix'] . ' €</td>';
                    echo '</tr>';
                }
            }
        }
        $liste_objets->closeCursor();
    }

    public function list_objets_by_user($id_user)
    {
        require('Encheres.php');
        $liste_objets = Objets::get_objets_by_user($id_user);
        while ($objet = $liste_objets->fetch())
        {
            if(self::vendu($objet['id_objet'])<2) {
                echo '<tr>';
                echo '<td><a href="page_objet.php?id_objet=' . $objet['id_objet'] . '">' . $objet['nom_objet'] . '</a></td>';
                echo '<td>' . $objet['desc_objet'] . '</td>';
                $encheres_controller = new Encheres();
                $encheres_infos = $encheres_controller->get_enchere_max($objet['id_objet']);
                while ($enchere_info = $encheres_infos->fetch()) {
                    echo '<td> ' . $enchere_info['prix'] . ' €</td>';
                }
                echo '<td><a href="../supprimer_objet_user.php?id_objet='. $objet['id_objet'].'"onclick="return confirm(\'Vérification de suppression?\');"><button class="btn btn-danger">Annuler</button></a></td>';
                echo '</tr>';
            }
        }
        $liste_objets->closeCursor();
    }

    public function list_objets_a_valider()
    {
        include_once('Users.php');
        $users_controller = new \quinenveut\Users();

        include_once('Encheres.php');
        $encheres_controller = new Encheres();

        $objets = new Objets();
        $liste_objets = $objets->get_objets();
        while ($objet = $liste_objets->fetch())
        {
            if ($objet['statut'] == 0)
            {
                echo '<tr>';
                echo '<td><a href="page_objet.php?id_objet=' . $objet['id_objet'] . '">' . $objet['nom_objet'] . '</a></td>';
                $vendeur_infos = $users_controller->get_user($objet['id_vendeur']);
                $vendeur = $vendeur_infos->fetch();
                echo '<td> ' . $vendeur['nom'] . '</td>';
                $encheres_infos = $encheres_controller->get_enchere_max($objet['id_objet']);
                $enchere_info = $encheres_infos->fetch();
                echo '<td> ' . $objet['prix_base'] . ' €</td>';
                echo '<td> ' . $objet['prix_res'] . ' €</td>';
                echo '<td> ' . $objet['desc_objet'] . '</td>';
                echo '<td><a href="../valider_objet.php?id_objet='.$objet['id_objet'].'"><button class="btn btn-success">Valider</button></a></td>';
                echo '<td><a href="../refuser_objet.php?id_objet='.$objet['id_objet'].'"><button class="btn btn-danger">Refuser</button></a></td>';
                echo '</tr>';
            }
        }
        $liste_objets->closeCursor();
    }

    public function ajout_vente() {
        require('database.php');

        $nom = strval($_POST["nom"]);
        $description = strval($_POST["description"]);
        $id_cat=intval($_POST["categorie"]);
        $prix_bas = intval($_POST["prix_bas"]);
        $prix_res = intval($_POST["prix_reserve"]);
        $date_limit=$_POST["date_limit"];
        $heure_limit=$_POST["heure_limit"];
        $visibilite = intval($_POST["visibilite"]);

        session_start();
        $id_vendeur = $_SESSION['id_user'];

        //ajouter le nouvel objet dans la base de donnees
        $sql = "INSERT INTO Objets(nom_objet, desc_objet, prix_base, prix_res, id_vendeur, id_cat, date_limit, heure_limit, id_visibilite, statut)
                        VALUES('$nom', '$description', '$prix_bas','$prix_res',  $id_vendeur, '$id_cat','$date_limit','$heure_limit', '$visibilite', 0)";

        if($bdd->query($sql)) {
            echo "<script>alert('Ajouté avec succès!');location.href='Vues/page_ventes.php'</script>";
        }
    }

    public function vendu($id_obj){
        include_once('Encheres.php');
        include_once('Users.php');

        $a=date('Y');
        $m=date('m');
        $d=date('d');
        $h=date('H');
        $min=date('i');
        $obj= $this->get_objet2($id_obj);
        if ($obj['statut']==1) {
            $jour = substr($obj['date_limit'], 8, 2);
            $mois = substr($obj['date_limit'], 5, 2);
            $annee = substr($obj['date_limit'], 0, 4);
            $heure = substr($obj['heure_limit'], 0, 2);
            $minute = substr($obj['heure_limit'], 3, 2);
            $stat=1;
            $limit=$annee.$mois.$jour.$heure.$minute;
            $date=$a.$m.$d.$h.$min;
            if ($date>=$limit){
                $ench=$this->get_enchere_max2($id_obj);
                $users_controller = new Users();
                $vnd=$users_controller->get_user2($obj['id_vendeur']);
                if ($ench){
                    $prix_actuel=$ench['prix'];
                    $acht=$users_controller->get_user2($ench['id_acheteur']);
                    if($prix_actuel>=$obj['prix_res']){

                        $this->send_mail($vnd['mail'],"Vente reussie",'Félicitations!! La vente de votre objet '.$obj['nom_objet'].' a été réalisé avec succés pour le prix de '.$ench['prix'].' euros, l\'acheteur est Mr '.$acht['nom'].' dont le mail est '.$acht['mail'].' .');
                        $this->send_mail($acht['mail'],"Achat reussie",'Félicitations!! l\'achat de l\'objet '.$obj['nom_objet'].' a ete réalisé avec succes, le vendeur est Mr/Mme '.$vnd['nom'].' , et dont le mail est '.$vnd['mail'].' .');
                        $this->update_statu($id_obj,2);
                        $stat=2;
                    }else{
                        $this->send_mail($vnd['mail'],"Vente annulée",'Nous avons le regret de vous informer que la vente de votre objet '.$obj['nom_objet'].' a été annulée car le prix de reserve na pas été atteint.');
                        $this->send_mail($acht['mail'],"Achat annulée",'Nous avons le regret de vous informer que l\'achat de l\'objet '.$obj['nom_objet'].' a été annulée car le prix de reserve na pas été atteint.');
                        $this->update_statu($id_obj,3);
                        $stat=3;
                    }
                }
                else {
                    $this->send_mail($vnd['mail'],"Vente annulée",'Nous avons le regret de vous informer que la vente de votre objet '.$obj['nom_objet'].' a été annulée car aucune enchere n\'a été proposé.');
                    $this->update_statu($id_obj,3);
                    $stat=3;
                }

            }
            return $stat;
        }
        else return $obj['statut'];
    }

    public function update_statu($id_obj,$stat){
        require('database.php');
        $sql="UPDATE Objets set statut='$stat' WHERE id_objet='$id_obj';";
        $bdd->query($sql);
    }

    public function valider_objet($id_objet)
    {
        require ('database.php');
        $requete = $bdd->prepare('UPDATE Objets set statut="1" WHERE id_objet= ? ;');
        $requete->execute(array($id_objet)) or die (print_r($requete->errorInfo()));
        header('Location: Vues/page_validation_objets.php');
    }

    public function supprimer_objet($id_objet)
    {
        require ('database.php');
        require_once ('Encheres.php');
        $ench=new Encheres();
        $ench->supprimer_encheres($id_objet);
        $requete = $bdd->prepare('DELETE FROM Objets WHERE id_objet= ? ;');
        $requete->execute(array($id_objet)) or die (print_r($requete->errorInfo()));
    }

    public function get_objet2($id_objet)
    {
        require('database.php');
        $liste_objets = $bdd->prepare('SELECT * FROM Objets WHERE id_objet = ? ;');
        $liste_objets->execute(array($id_objet)) or die (print_r($liste_objets->errorInfo()));
        return $liste_objets->fetch();
    }

    public function send_mail($mailto,$mailsub,$mesg){
        if (file_exists("Mail.php"))
            include_once('Mail.php');
        else
            include_once('../Mail.php');
        $mail=new Mail();
        $mail->send_mail($mailto,$mailsub,$mesg);

    }
    public function supprimer_objet_user($id_user)
    {
        require ('database.php');
        require_once ('Encheres.php');
        $ench=new Encheres();
        $objets=$this->get_objets_by_user($id_user);
        while ($obj=$objets->fetch()){
            $ench->supprimer_encheres($obj['id_objet']);
            $requete = $bdd->prepare('DELETE FROM Objets WHERE id_objet= ? ;');
            $requete->execute(array($obj['id_objet'])) or die (print_r($requete->errorInfo()));
        }

    }
    public function get_enchere_max2($id){
        require_once('Encheres.php');
        $encheres_controller = new Encheres();
        $ench=$encheres_controller->get_enchere_max_vendu($id);
        return $ench;
    }

    public  function count_objets_a_valider(){

        $objets = new Objets();
        $result =0;
        $liste_objets = $objets->get_objets();

        while ($objet = $liste_objets->fetch())
        {
            if ($objet['statut'] == 0) {

                $result =$result+1;
            }

            }
            return $result;


    }

    public  function count_objets_vendu(){

        $objets = new Objets();
        $result =0;
        $liste_objets = $objets->get_objets();

        while ($objet = $liste_objets->fetch())
        {
            if ($objet['statut'] == 2) {

                $result =$result+1;
            }

        }
        return $result;


    }

    public  function count_objets_en_cours(){

        $objets = new Objets();
        $results =0;
        $liste_objets = $objets->get_objets();

        while ($objet = $liste_objets->fetch())
        {
            if ($objet['statut'] <> 0) {

                $results =$results+1;
            }

        }
        return $results;


    }
}
?>
