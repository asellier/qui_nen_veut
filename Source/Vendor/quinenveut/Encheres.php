<?php

namespace quinenveut;

class Encheres
{
    public function __construct()
    {
        include_once('database.php');
    }

    public function get_encheres()
    {
        require('database.php');
        $liste_encheres = $bdd->query('SELECT * FROM Encheres;') or die (print_r($liste_encheres->errorInfo()));
        return $liste_encheres;
    }
    public function get_encheres_by_id($id_encheres)
    {
        require('database.php');
        $liste_echeres = $bdd->prepare('SELECT * FROM Encheres WHERE id_ench = ? ;');
        $liste_echeres->execute(array($id_encheres)) or die (print_r($liste_echeres->errorInfo()));
        return $liste_echeres;
    }

    public function get_encheres_by_user($id_user)
    {
        require('database.php');
        $liste_encheres = $bdd->prepare('SELECT * FROM Encheres WHERE id_acheteur = ? ;');
        $liste_encheres->execute(array($id_user)) or die (print_r($liste_encheres->errorInfo()));
        return $liste_encheres;
    }

    public function get_encheres_by_objet($id_objet)
    {
        require('database.php');
        $liste_encheres = $bdd->prepare('SELECT * FROM Encheres WHERE id_objet = ? ;');
        $liste_encheres->execute(array($id_objet)) or die (print_r($liste_encheres->errorInfo()));
        return $liste_encheres;
    }

    public function get_enchere_max($id_objet)
    {
        require('database.php');
        // $liste_encheres = $bdd->prepare('SELECT MAX(prix) as prix, id_acheteur FROM Encheres WHERE id_objet = ? ;');
        $liste_encheres = $bdd->prepare('SELECT MAX(prix) as prix FROM Encheres WHERE id_objet = ? ;');
        $liste_encheres->execute(array($id_objet)) or die (print_r($liste_encheres->errorInfo()));
        return $liste_encheres;
    }

    public function list_all_encheres ()
    {
        require('database.php');
        $liste_encheres = Encheres::get_encheres();
        while ($enchere = $liste_encheres->fetch())
        {
            echo '<tr><td>'.$enchere['id_objet'].'</td><td> '.$enchere['prix'].' €</td></tr>';
        }
        $liste_encheres->closeCursor();
    }

    //Version Aurélie, en attendant Manel
    /*public function list_encheres_by_user ($id_user)
    {
        require('Objets.php');
        $liste_encheres = Encheres::get_encheres_by_user($id_user);
        while ($enchere = $liste_encheres->fetch())
        {
            $objets_controller = new Objets();
            $objets_infos = $objets_controller->get_objet($enchere['id_objet']);
            while ($objet_info = $objets_infos->fetch())
            {
                if ($objet_info["statut"]<2){
                    echo '<tr><td>'.$objet_info["nom_objet"].'</td><td>'.$enchere["prix"].' €</td><td><a href="page_info_enchere.php?id_ench='.$enchere["id_ench"].'">Info</a></td></tr>';
                }
            }
        }
        $liste_encheres->closeCursor();
    }*/

    //Version Manel
    public function list_encheres_by_user ($id_user)
    {
        require('Objets.php');
        $liste_encheres = Encheres::get_encheres_by_user($id_user);
        while ($enchere = $liste_encheres->fetch())
        {
            $objets_controller = new Objets();
            $objets_infos = $objets_controller->get_objet($enchere['id_objet']);
            while ($objet_info = $objets_infos->fetch())
            {
                if ($objet_info["statut"]<2){
                    echo '<tr><td>'.$objet_info["nom_objet"].'</td><td>'.$enchere["prix"].' €</td><td><a href="page_info_enchere.php?id_ench='.$enchere["id_ench"].'">Info</a></td></tr>';
                }
            }
        }
        $liste_encheres->closeCursor();
    }

    public function ajout_vente() {
        require('database.php');

        $nom = strval($_POST["nom"]);
        $description = strval($_POST["description"]);
        $prix_bas = intval($_POST["prix_bas"]);
        $prix_res = intval($_POST["prix_reserve"]);

        //ajouter le nouvel object dans la base de donnees
        $sql = "INSERT INTO Objets(nom_objet, desc_objet, prix_base, prix_res, id_vendeur, id_cat)
                        VALUES('$nom', '$description', '$prix_bas','$prix_res',  2, 1)";

        if($bdd->query($sql)) {
            echo "<script>alert('Ajouté avec succès!');location.href='Vues/page_accueil.php'</script>";
        }
    }

    public function checkmontantencher(){

        require('database.php');
        $idobjet= $_POST['id_objet'];
        $idacheteur= $_POST['id_acheteur'];
        $montant = $_POST["montant"];

        require ('Objets.php');
        $controller_objets = new \quinenveut\Objets;
        $objet = $controller_objets->get_objet($idobjet);
        $infos_objet = $objet->fetch();
        $montant_base = $infos_objet['prix_base'];


        // Ajout d'encher dans la base de donnee
        if($montant>$montant_base) {
            $sql = "INSERT INTO Encheres(prix, id_objet, id_acheteur)
                            VALUES('$montant', '$idobjet', '$idacheteur')";
            $bdd->query($sql);
            return 1;

        }
        else{

            return 0;

        }


    }

    public function encherir(){
        $res = $this->checkmontantencher();

        if ($res==1) {
            $add= 'Vues/page_encheres.php';
            echo "<script>alert('Enchére Acceptée');location.href='$add'</script>";
            return $add;

        }
        else if($res==0){

            $add= 'Vues/formulaire_enchere.php';
            echo "<script>alert('Enchére Non Acceptée');location.href='$add'</script>";
            return $add;

        }

    }

    public function encherir_visiteur(){

        require('database.php');
        $idobjet= $_POST['id_objet'];
        $montant = $_POST["montant"];

        $encheres_controller = new Encheres();
        $encheres_infos = $encheres_controller->get_enchere_max($idobjet['id_objet']);
        $enchere_info = $encheres_infos->fetch();

        if($montant>$enchere_info['prix']){

            $requete = $bdd->prepare('UPDATE Encheres set prix='.$montant.' WHERE id_objet= ? ;');
            $requete->execute(array($idobjet)) or die (print_r($requete->errorInfo()));

            $add= 'Vues/page_accueil.php';
            echo "<script>alert('Enchére Acceptée Vous avez entrer un prix superieur Aux Meilleur Enchéres');location.href='$add'</script>";
            return $add;

        }
        else{
            $add= 'Vues/page_accueil.php';
            echo "<script>alert('Enchére Non Acceptée , Entrez un prix superieur Aux Meilleur Enchéres');location.href='$add'</script>";
            return $add;

        }



        require ('Objets.php');
        $controller_objets = new \quinenveut\Objets;
        $objet = $controller_objets->get_objet($idobjet);
        $infos_objet = $objet->fetch();
        $montant_base = $infos_objet['prix_base'];


    }

    public function get_enchere_max_vendu($id_objet)
    {
        require('database.php');
        $ench=$this->get_enchere_max($id_objet);
        if($ench=$ench->fetch()){
            $liste_encheres = $bdd->prepare('select id_acheteur,prix from Encheres where id_objet= ? and prix= ? ;');
            //$liste_encheres = $bdd->prepare('SELECT MAX(prix) as prix FROM Encheres WHERE id_objet = ? ;');
            $liste_encheres->execute(array($id_objet,$ench['prix'])) or die (print_r($liste_encheres->errorInfo()));
        }
        else $liste_encheres=$ench;

        return $liste_encheres->fetch();
    }
    public function supprimer_encheres($id_objet)
    {
        require ('database.php');
        $requete = $bdd->prepare('DELETE FROM Encheres WHERE id_objet= ? ;');
        $requete->execute(array($id_objet)) or die (print_r($requete->errorInfo()));
    }
    public function supprimer_encheres_user($id_user)
    {
        require ('database.php');
        $requete = $bdd->prepare('DELETE FROM Encheres WHERE id_acheteur= ? ;');
        $requete->execute(array($id_user)) or die (print_r($requete->errorInfo()));
    }
}

?>
