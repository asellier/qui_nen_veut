<?php
include_once('header.php');
$id_objet=$_GET['id_objet'];
$id_acheteur = $_SESSION['id_user'] ;
include_once('details_objet.php');
?>
<form name="enchere" action="../encherir.php" method="post">
	<div class="row">
		<div class="col-lg-4 col-md-offset-4 margin">
			<div class="input-group">
				<span class="input-group-addon" id="basic-addon1">Votre offre</span>
				<input type="text" id="montant" name="montant" class="form-control" placeholder="Montant de votre offre" required>
				<input  type="text" name="id_objet" value="<?php echo $id_objet?> " hidden />
				<input  type="text" name="id_acheteur" value="<?php echo $id_acheteur?>" hidden />

				<span class="input-group-btn">
	  				<button type="submit" class="btn btn-success">Confirmer</button>
				</span>
			</div>
		</div>
	</div>
</form>
<?php
include_once('footer.php');
?>
