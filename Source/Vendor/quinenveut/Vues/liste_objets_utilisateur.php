<div class="row">
    <section class="col-sm-8 col-md-offset-1 margin">
        <table class="table table-bordered table-striped table-condensed">
            <caption>
                <h4>Liste des objets en vente</h4>
            </caption>
            <thead>
            <tr>
                <th>Objet</th>
                <th>Description</th>
                <th>Meilleure enchère</th>
                <th></th>
            </tr>
            </thead>
            <tbody>
<?php
require_once('../Objets.php');
$objets_controller = new \quinenveut\Objets();
$objets_controller->list_objets_by_user($_SESSION['id_user']);
?>
            </tbody>
        </table>
    </section>
</div>
