DROP DATABASE IF EXISTS m2test7;
CREATE DATABASE m2test7
    CHARACTER SET utf8mb4
    COLLATE utf8mb4_unicode_ci;
USE m2test7;

CREATE TABLE Roles(
  id_role int(2) NOT NULL,
  nom_role varchar(45) DEFAULT NULL,
  PRIMARY KEY (id_role)
) DEFAULT CHARSET=utf8;

CREATE TABLE Users(
  id_user int(11) NOT NULL AUTO_INCREMENT,
  id_role int(2) NOT NULL,
  mail varchar(45) NOT NULL,
  mdp varchar(45) DEFAULT NULL,
  nom varchar(45) DEFAULT NULL,
  prenom varchar(45) DEFAULT NULL,
  statut tinyint(1) DEFAULT NULL, -- utilisateur valide ou non
  adresse varchar(200) DEFAULT NULL,
  PRIMARY KEY (id_user),
  FOREIGN KEY (id_role) REFERENCES Roles(id_role)
) DEFAULT CHARSET=utf8;


CREATE TABLE Categories(
	id_cat int(11) NOT NULL AUTO_INCREMENT,
	nom_cat varchar(45) DEFAULT NULL,
  PRIMARY KEY (id_cat)
) DEFAULT CHARSET=utf8;


CREATE TABLE Visibilite(
  id_visibilite int(2) NOT NULL,
  nom_visibilite varchar(45) DEFAULT NULL,
  PRIMARY KEY (id_visibilite)
) DEFAULT CHARSET=utf8;

CREATE TABLE Objets(
  id_objet int(11) NOT NULL AUTO_INCREMENT,
  nom_objet varchar(45) DEFAULT NULL,
  desc_objet varchar(45) DEFAULT NULL,
  prix_base float DEFAULT NULL,
  prix_res float DEFAULT NULL,
  date_limit date DEFAULT NULL,
  heure_limit varchar(45) DEFAULT NULL,
  id_cat int(11) NOT NULL,
  photo BLOB DEFAULT NULL,
  id_vendeur int(11) NOT NULL,
  statut tinyint(1) DEFAULT NULL, -- objey validé ou non
  id_visibilite int(2) DEFAULT NULL,
  PRIMARY KEY (id_objet),
  FOREIGN KEY (id_cat) REFERENCES Categories(id_cat),
  FOREIGN KEY (id_vendeur) REFERENCES Users(id_user),
  FOREIGN KEY (id_visibilite) REFERENCES Visibilite(id_visibilite)
) DEFAULT CHARSET=utf8;

CREATE TABLE Encheres(
  id_ench int(11) NOT NULL AUTO_INCREMENT,
  prix double DEFAULT NULL,
  id_objet int(11) NOT NULL,
  id_acheteur int(11) NOT NULL,
  PRIMARY KEY (id_ench),
  FOREIGN KEY (id_objet) REFERENCES Objets(id_objet),
  FOREIGN KEY (id_acheteur) REFERENCES Users(id_user)
 ) DEFAULT CHARSET=utf8;

CREATE TABLE Invitations(
  id_invit int(11) NOT NULL AUTO_INCREMENT,
  id_objet int(11) NOT NULL,
  id_user int(11) NOT NULL,
  PRIMARY KEY (id_invit),
  FOREIGN KEY (id_objet) REFERENCES Objets(id_objet),
  FOREIGN KEY (id_user) REFERENCES Users(id_user)
) DEFAULT CHARSET=utf8;
