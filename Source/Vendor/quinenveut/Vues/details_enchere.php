<?php
$id_encheres = $_GET['id_ench'];

require('../Encheres.php');
$encheres_controller = new \quinenveut\Encheres();
$infos_enchere = $encheres_controller->get_encheres_by_id($id_encheres);
$enchere = $infos_enchere->fetch();

require_once('../Objets.php');
$objets_controller = new \quinenveut\Objets();
$infos_objet = $objets_controller->get_objet($enchere['id_objet']);
$objet = $infos_objet->fetch();

require_once('../Users.php');
$users_controller = new \quinenveut\Users();
$infos_acheteur = $users_controller->get_user($enchere['id_acheteur']);
$acheteur = $infos_acheteur->fetch();
?>
<html>
    <head>
        <title>Page Enchere</title>
    </head>
    <body>
        <br><br><br><br><br>
        <div class="col-md-12 col-md-offset-2 margin">
            <ul class="list-group col-sm-8" >
                <li class="list-group-item active"> Informations sur votre enchere</li>
                <li class="list-group-item"><b>Objet : </b><?php echo $objet['nom_objet'] ?></li>
                <li class="list-group-item"><b>Description : </b><?php echo $objet['desc_objet'] ?></li>
                <li class="list-group-item"><b>Prix proposé : </b><?php echo $enchere['prix'] ?> €</li>
                <li class="list-group-item"><b>Acheteur : </b><?php echo $acheteur['nom'] ?></li>
                <br />
            </ul>
        </div>
        <br><br>
        <a href="page_encheres.php"><button class="btn btn-primary">Précédent</button></a>
    </body>
</html>
