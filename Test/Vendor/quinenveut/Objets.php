<?php

namespace quinenveut\tests\units;
use \atoum;

class Objets extends atoum
{

    public function testVenteReussi(){
        $this

            ->given($users = new \Mock\quinenveut\Users())
            ->and($objet = new \Mock\quinenveut\Objets())
            ->and($obj=array('id_objet'=>1,'prix_base'=>0,'prix_res'=>1,'id_vendeur'=>1,'date_limit'=>"2017-10-01",'heure_limit'=>'14:30','statut'=>1,'nom_objet'=>'objet de test',))
            ->and($ench=array("id_objet"=>1,"prix"=>99999,"id_acheteur"=>1,))
            ->and($usr=array("mail"=>"lyestairi@gmail.com",'nom'=>'Djamal',))
            //mock

            ->and($this->calling($objet)->send_mail=1)
            ->and($this->calling($objet)->get_enchere_max2=$ench)
            ->and($this->calling($objet)->get_objet2=$obj)
            ->and($this->calling($users)->get_user2=$usr)

            ->then
            ->integer($objet->vendu(1))
            ->isEqualTo(2)
        ;
    }

    public function testVenteannule(){
        $this

            ->given($users = new \Mock\quinenveut\Users())
            ->and($objet = new \Mock\quinenveut\Objets())
            ->and($obj=array('id_objet'=>1,'prix_base'=>0,'prix_res'=>99999,'id_vendeur'=>1,'date_limit'=>"2017-10-01",'heure_limit'=>'14:30','statut'=>1,'nom_objet'=>'objet de test',))
            ->and($ench=array("id_objet"=>1,"prix"=>1,"id_acheteur"=>1,))
            ->and($usr=array("mail"=>"lyestairi@gmail.com",))
            //mock

            ->and($this->calling($objet)->send_mail=1)
            ->and($this->calling($objet)->get_objet2=$obj)
            ->and($this->calling($users)->get_user2=$usr)

            ->then
            ->integer($objet->vendu(1))
            ->isEqualTo(3)
        ;
    }
    public function testVentePasFini(){
        $this

            ->given($users = new \Mock\quinenveut\Users())
            ->and($objet = new \Mock\quinenveut\Objets())
            ->and($obj=array('id_objet'=>1,'prix_base'=>0,'prix_res'=>99999,'id_vendeur'=>1,'date_limit'=>"2018-10-01",'heure_limit'=>'14:30','statut'=>1,'nom_objet'=>'objet de test',))
            ->and($ench=array("id_objet"=>1,"prix"=>1,"id_acheteur"=>1,))
            ->and($usr=array("mail"=>"lyestairi@gmail.com",))
            //mock

            ->and($this->calling($objet)->send_mail=1)
            ->and($this->calling($objet)->get_objet2=$obj)
            ->and($this->calling($users)->get_user2=$usr)

            ->then
            ->integer($objet->vendu(1))
            ->isEqualTo(1)
        ;
    }
}