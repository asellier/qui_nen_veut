<?php
	include_once('header.php');
?>

    <html>
    <head>
        <title>Modification</title>
        <style>
            .col-center-block {
                float: none;
                display: block;
                margin-left: auto;
                margin-right: auto;
            }
        </style>
    </head>

    <body>
    <?php
        $nom_cat = $_POST["nom_cat"];
        $id_cat = $_POST["id_cat"];
        $_SESSION['id_cat'] = $id_cat;
    ?>
    <div class="container">
        <div class="row myCenter">
            <div class="col-xs-6 col-md-4 col-center-block">
                <form class="form-signin" method="post" action="../modif_categorie.php">
                    <div class="row">
                        <div class="center-block" >
                            <h1 class="red-text text-center">  <br/><small class="red-text text-center"> Modifier le nom de catégorie </small></h1>
                            <br/><br/>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="catnom" class="sr-only">Saisie nom</label>
                        <input type="text" id="nom" name="nom" class="form-control" value="<?php echo $nom_cat?>" required autofocus> <br>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-lg btn-primary btn-block" type="submit">valider</button> <br>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-lg btn-primary btn-block" onclick="location.href='page_admin_categories.php'">Retour</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
    </body>
    </html>

<?php
	include_once('footer.php');
?>