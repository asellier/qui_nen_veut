<?php

include_once('header.php');
include_once('../Users.php');
$user = new \quinenveut\Users();
$user=$user->get_user2($_GET['id_user']);
?>
<html>
<head>
    <title>Profil</title>
</head>
<body>
<br><br><br><br><br>
<div class="col-md-12 col-md-offset-2 margin">
    <ul class="list-group col-sm-8" >
        <li class="list-group-item active"> Informations sur l'utilisateur</li>
        <li class="list-group-item"><b>Nom : </b><?php echo $user['nom'] ?></li>
        <li class="list-group-item"><b>Prenom : </b><?php echo $user['prenom'] ?></li>
        <li class="list-group-item"><b>Adresse mail : </b><?php echo $user['mail'] ?></li>
        <li class="list-group-item"><b>Adresse: </b><?php echo $user['adresse'] ?></li>
        <br />
    </ul>
</div>
<div class="row">
    <a href="page_admin_users.php"><button type="button" class="btn btn-warning">Gestion des utilisateurs</button></a>
</div>
</body>
</html>
<?php
include_once('footer.php');

?>
