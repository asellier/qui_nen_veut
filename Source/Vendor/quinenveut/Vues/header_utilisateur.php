                <li><a href="page_categories.php"><span class="glyphicon glyphicon-list"></span> Catégories</a></li>
                <li><a href="formulaire_vente.php"><span class="glyphicon glyphicon-edit"></span> Créer une enchère</a></li>
                <li><a href="page_ventes.php"><span class="glyphicon glyphicon-piggy-bank"></span> Mes objets en vente</a></li>
                <li><a href="page_encheres.php"><span class="glyphicon glyphicon-euro"></span> Mes enchères</a></li>
            </ul>
            <ul class="nav navbar-nav navbar-right">
                <li><a href="page_profil.php?id_user=<?php echo $_SESSION['id_user'] ?>"><span class="glyphicon glyphicon-user"></span> Mon compte</a></li>
                <li><a href="../deconnexion.php"><span class="glyphicon glyphicon-log-out"></span> Se déconnecter</a></li>
