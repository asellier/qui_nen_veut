<?php
session_start();
?>

<!DOCTYPE html>
<html lang="fr">
<head>
    <title>Qui n'en veut !?</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <script src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">

    <style>
        .navbar {
            margin-bottom: 0;
            border-radius: 0;
        }

        .row.content {
            height: 450px
        }
        .glyphicon-remove { color: red}

        html {
            position: relative;
            min-height: 100%;
        }
        body {
            margin-bottom: 60px;
        }

        footer {
            background-color: lightgrey;
            color: #darkgrey;
            padding: 15px;
            position: absolute;
            bottom: 0;
            width: 100%;
            height: 60px;
        }

        th {
            text-align: center;
        }

        @media screen and (max-width: 767px) {
            .sidenav {
                height: auto;
                padding: 15px;
            }

            .row.content {
                height:auto;
            }
        }

    </style>
</head>
<body>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="page_accueil.php">Qui n'en veut</a>
        </div>
        <div class="collapse navbar-collapse" id="myNavbar">
            <ul class="nav navbar-nav">
			    <li><a href="page_accueil.php"><span class="glyphicon glyphicon-home"></span> Accueil</a></li>
<?php

//Récupération des variables de session

if (!isset($_SESSION['id_user']))
{
    include_once('header_visiteur.php');
}
else
{
    if ($_SESSION['role'] == 1)
    {
        include_once('header_admin.php');
    }
    else {
        include_once('header_utilisateur.php');
    }
}
?>
			</ul>
        </div>
    </div>
</nav>

<div class="container-fluid text-center">
    <div class="row content">
