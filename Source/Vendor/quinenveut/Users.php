<?php

namespace quinenveut;

class Users
{
    public function get_user($id_user)
    {
        require('database.php');
        $utilisateur = $bdd->prepare('SELECT * FROM Users WHERE id_user = ? ;');
        $utilisateur->execute(array($id_user)) or die (print_r($utilisateur->errorInfo()));
        return $utilisateur;
    }

    public function get_user_by_mail($mail)
    {
        require('database.php');
        $utilisateur = $bdd->prepare('SELECT * FROM Users WHERE mail = ? ;');
        $utilisateur->execute(array($mail)) or die (print_r($utilisateur->errorInfo()));
        return $utilisateur;
    }

    public function get_users()
    {
        require('database.php');
        $liste_utilisateurs = $bdd->query('SELECT * FROM Users;') or die (print_r($liste_utilisateurs->errorInfo()));
        return $liste_utilisateurs;
    }

    public function list_all_users()
    {
        require('database.php');
        $liste_utilisateurs = Users::get_users();
        while ($utilisateur = $liste_utilisateurs->fetch())
        {
            echo '<tr><td>'.$utilisateur['nom'].'</td><td> '.$utilisateur['mail'].'</td></tr>';
        }
        $liste_utilisateurs->closeCursor();
    }

    public function list_all_users_admin()
    {
        $liste_users = Users::get_users();
        return $liste_users;
    }

    //Vérification des login / mdp
    public function check(){
        require('database.php');

        $mdp_saisi = $_POST['password'];

        $utilisateur = Users::get_user_by_mail($_POST['email']);
        $infos_utilisateur = $utilisateur->fetch();
        $mdp_bd = $infos_utilisateur['mdp'];
        $id_user = $infos_utilisateur['id_user'];

        if ($mdp_saisi == $mdp_bd) {
            if ($infos_utilisateur['statut']==0) {
                return 2;
            }
            else {
                //Création des variables de session
                session_start();
                $_SESSION['id_user'] = $id_user;
                $_SESSION['mail'] = $_POST['email'];
                $_SESSION['role'] = $infos_utilisateur['id_role'];
                return 1;
            }
        }else{
            return 0;
        }

    }

    public function connecter()
    {
        $res = $this->check();

        if ($res == 1)
        {
            $add = 'Vues/page_accueil.php';
            echo "<script>alert('Connecté avec succès');location.href='$add'</script>";
            return $add;
        }
        else if($res==0)
        {
            $add = 'Vues/formulaire_connexion.php';
            echo "<script>alert('Email ou mot de passe incorrect !');location.href='$add'</script>";
            return $add;
        }
        else if($res==2){
            $add = 'Vues/page_accueil.php';
            echo "<script>alert('Le compte est en attente de validation!');location.href='$add'</script>";
            return $add;
        }
    }

    //Vérification des infos inscription
    public function check2(){
        require('database.php');

        $nom = strval($_POST["nom"]);
        $prenom = strval($_POST["prenom"]);
        $email = strval($_POST["email"]);
        $password1 = strval($_POST["password"]);
        $password2 = strval($_POST["passconf"]);
        $adresse = strval($_POST["adresse"]);

        //verifier si les 2 passwords sont memes
        if($password1 == $password2) {
            //verifier si email existe deja
            $utilisateur = Users::get_user_by_mail($email);
            if($usr=$utilisateur->fetch()){
                //si le compte n est pas encore valide
                if ($usr['statut']==0){
                    return 2;
                }
                else return -1;
            } else{
                //ajouter le nouvel utilisateur dans la base de donnees
                $sql = "INSERT INTO Users(id_role, mail, mdp, nom, prenom, adresse, statut)
                        VALUES(2, '$email', '$password1','$nom', '$prenom', '$adresse','0')";
                $bdd->query($sql);

                //obtenir le id_user
                /*$utilisateur = Users::get_user_by_mail($email);
                $infos_utilisateur = $utilisateur->fetch();
                $id_user = $infos_utilisateur['id_user'];

                //Création des variables de session
                session_start();
                $_SESSION['id_user'] = $id_user;
                $_SESSION['mail'] = $email;
                $_SESSION['role'] = $infos_utilisateur['id_role'];*/
                return 1;
            }
        }else{
            return 0;
        }

    }

    public function inscription() {

        $res = $this->check2();

        //email existe deja
        if($res == -1) {
            $add = 'Vues/formulaire_inscription.php';
            echo "<script>alert('Le mail existe déjà!');location.href='$add'</script>";
            return $add;
        } else if($res == 0){   //les 2 passwords ne sont pas memes
            $add = 'Vues/formulaire_inscription.php';
            echo "<script>alert('Les 2 passwords ne sont pas mêmes!');location.href='$add'</script>";
            return $add;
        } else if($res == 1) {  //inscription avec succes
            $add = 'Vues/page_accueil.php';
            echo "<script>alert('Enregistré avec succès! En attente de la validation par l\'administrateur !');location.href='$add'</script>";
            return $add;
        }
        else if($res==2){
            $add = 'Vues/page_accueil.php';
            echo "<script>alert('Le compte correspondant a ce mail est en attente de validation!');location.href='$add'</script>";
            return $add;
        }
    }

    public function deconnecter()
    {
        session_start();
        unset($_SESSION['id_user']);
        session_destroy();
        header('Location: Vues/page_accueil.php');
        exit();
    }
    public function get_user2($id_user)
    {
        require('database.php');
        $utilisateur = $bdd->prepare('SELECT * FROM Users WHERE id_user = ? ;');
        $utilisateur->execute(array($id_user)) or die (print_r($utilisateur->errorInfo()));
        return $utilisateur->fetch();
    }
    public function valider_user($id){
        require_once('database.php');
        $sql="UPDATE Users set statut=1 WHERE id_user='$id';";
        $bdd->query($sql);


    }
    public function supprimer_user($id){
        require_once('database.php');
        require_once ('Objets.php');
        require_once ('Encheres.php');
        $objet=new Objets();
        $ench=new Encheres();
        $ench->supprimer_encheres_user($id);
        $objet->supprimer_objet_user($id);
        $requete = $bdd->prepare('DELETE FROM Users WHERE id_user= ? ;');
        $requete->execute(array($id)) or die (print_r($requete->errorInfo()));
    }


    public  function count_user_a_valider(){

        $users = new Users();
        $result =0;
        $liste_user = $users->get_users();

        while ($users = $liste_user->fetch())
        {
            if (($users['statut'] == 0) and ($users['id_role'] == 2)) {

                $result =$result+1;
            }

        }
        return $result;


    }

}
?>
