<?php
namespace quinenveut\tests\units;

use \atoum;


class Encheres extends atoum
{


    public function testEncherirSuccess(){
        $this
            ->given($ench = new \Mock\quinenveut\Encheres())
            ->and($res = 1)

            ->and($this->calling($ench)->checkmontantencher = $res)

            ->then
            ->string($ench->encherir())
            ->isIdenticalTo('Vues/page_encheres.php')
        ;

    }

    public function testEncherirError(){
        $this
            ->given($ench = new \Mock\quinenveut\Encheres())
            ->and($res = 0)

            ->and($this->calling($ench)->checkmontantencher = $res)

            ->then
            ->string($ench->encherir())
            ->isIdenticalTo('Vues/formulaire_enchere.php')
        ;

    }


}