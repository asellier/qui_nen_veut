<!DOCTYPE html>
<html>
<head>
    <script>
        function del() {
            var msg = "Vérification de suppression";
            if (confirm(msg)==true){
                return true;
            }else{
                return false;
            }
        }
    </script>
</head>
<body>
<?php
include_once('header.php');
?>



<div class="row">
    <section class="col-sm-8 col-md-offset-1 margin">
        <table class="table table-bordered table-striped table-condensed">
            <caption>
                <h4>Liste des utilisateurs a valider</h4>
            </caption>
            <thead>
            <tr>
                <th>Mail</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th>Adresse</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            require_once('../Users.php');
            $users_controller = new \quinenveut\Users();
            $users = $users_controller->get_users();
            $users=$users->fetchAll();

            foreach ($users as $user) {
                if (($user['statut'] == 0) and ($user['id_role'] == 2)) {
                    echo '<tr><td>' . $user['mail'] . '</td><td> ' . $user['nom'] . '</td><td> ' . $user['prenom'] . '</td><td> ' . $user['adresse'] . '</td>';
                    echo '<td><a href="../valider_user.php?id_user=' . $user['id_user'] . '"><button class="btn btn-success">Valider</button></a></td>';
                    echo '<td><a href="../refuser_user.php?id_user=' . $user['id_user'] . '"><button class="btn btn-danger">Refuser</button></a></td></tr>';
                }
            }
            ?>
            </tbody>
        </table>
        <div class="row">
            <a href="page_admin_users.php"><button class="btn btn-primary">Retour</button></a>
        </div>
        <br/>

    </section>

</div>



<?php
include_once('footer.php');
?>
</body>
</html>
