<?php
namespace quinenveut\tests\units;

use \atoum;


class Mail extends atoum
{

    public function testSendInviatationSuccess(){
        $this
            ->given($send = new \Mock\quinenveut\Mail())
            ->and($res = 1)

            ->and($this->calling($send)->checksendinvitation = $res)

            ->then
            ->string($send->send_invitation())
            ->isIdenticalTo('Vues/page_accueil.php')
        ;

    }

    public function testSendInviatationError(){
        $this
            ->given($send = new \Mock\quinenveut\Mail())
            ->and($res = 0)

            ->and($this->calling($send)->checksendinvitation = $res)

            ->then
            ->string($send->send_invitation())
            ->isIdenticalTo('Vues/formulaire_invitation.php')
        ;

    }

}