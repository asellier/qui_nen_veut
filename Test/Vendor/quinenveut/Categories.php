<?php
namespace quinenveut\tests\units;

use \atoum;


class Categories extends atoum
{
    // la categorie est modifiee dans la base de donnees
    public function testModifCategorie(){
        $this
            ->given($categorie = new \Mock\quinenveut\Categories())
            ->and($res = 'UPDATE Categories SET nom_cat=\'fleur  \' WHERE id_cat=\'20 \'')

            ->and($this->calling($categorie)->update_cat = $res)

            ->then
            ->string($categorie->modif_categorie())
            ->isIdenticalTo('Vues/page_admin_categories.php')
        ;
    }

    // la categorie n'est pas modifiee
    public function testModifCategorieError(){
        $this
            ->given($categorie = new \Mock\quinenveut\Categories())
            ->and($res = false)

            ->and($this->calling($categorie)->update_cat = $res)

            ->then
            ->string($categorie->modif_categorie())
            ->isIdenticalTo('Vues/formulaire_modif_categories.php')
        ;
    }

    // la categorie est supprimee dans la base de donnees
    public function testSupprCategorie(){
        $this
            ->given($categorie = new \Mock\quinenveut\Categories())
            ->and($res = 'DELETE FROM Categories WHERE id_cat=\'20 \'')

            ->and($this->calling($categorie)->delete_cat = $res)

            ->then
            ->integer($categorie->suppr_categorie())
            ->isIdenticalTo(1)
        ;
    }

    // la categorie n'est pas supprimee
    public function testSupprCategorieError(){
        $this
            ->given($categorie = new \Mock\quinenveut\Categories())
            ->and($res = false)

            ->and($this->calling($categorie)->delete_cat = $res)

            ->then
            ->integer($categorie->suppr_categorie())
            ->isIdenticalTo(0)
        ;
    }

    // la categorie est ajoutee dans la base de donnees
    public function testAjoutCategorie(){
        $this
            ->given($categorie = new \Mock\quinenveut\Categories())
            ->and($res = 'INSERT INTO Categories(nom_cat) VALUES( \'xbox\')')

            ->and($this->calling($categorie)->insert_cat = $res)

            ->then
            ->string($categorie->ajout_categorie())
            ->isIdenticalTo('Vues/page_admin_categories.php')
        ;
    }

    // la categorie n'est pas ajoutee dans la base de donnees
    public function testAjoutCategorieError(){
        $this
            ->given($categorie = new \Mock\quinenveut\Categories())
            ->and($res = false)

            ->and($this->calling($categorie)->insert_cat = $res)

            ->then
            ->string($categorie->ajout_categorie())
            ->isIdenticalTo('Vues/formulaire_ajout_categorie.php')
        ;
    }
}