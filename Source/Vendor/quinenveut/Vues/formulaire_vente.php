<?php
include_once('header.php');
include_once('../Categories.php');
$categories_controller = new \quinenveut\Categories();
$categories = $categories_controller->get_categories();
?>

<html>
    <head>
        <title>Enchere</title>
        <style>
            .col-center-block {
                float: none;
                display: block;
                margin-left: auto;
                margin-right: auto;
            }
        </style>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        <script>
            function validateForm() {
                var pr = document.forms["form1"]["prix_reserve"].value;
                var pb = document.forms["form1"]["prix_bas"].value;
                var dl = document.forms["form1"]["date_limit"].value;
                var date = new Date();
                var month = date.getMonth()+1;
                var day = date.getDate();
                var year = date.getFullYear();
                if (month< 10)
                    month = "0"+month;
                if (day < 10)
                    day = "0"+day;
                var plop=''+year+month+day;
                dl=''+dl.substr(0,4)+dl.substr(5,2)+dl.substr(8,2);
                var k=0;
                var s='';
                if (parseInt(dl) <= parseInt(plop)) {
                    s="La date limite doit etre superieure a celle d'aujordhui !";
                    k=1
                }

                if (parseInt(pr) <= parseInt(pb)) {
                    s=s+"\n"+"Le prix de reserve doit etre superieur au prix de base!";
                    k=1;
                }
                if (k==1) {
                    alert(s);
                    return false;
                }
            }
        </script>
    </head>
    <body>
        <div class="container">
            <div class="row myCenter">
                <div class="col-xs-6 col-md-4 col-center-block">
                    <form name="form1" class="form-signin" onsubmit="return validateForm()" action="..\ajout_vente.php"method="post" >
                        <div class="row">
                            <div class="center-block">
                                <h1 class="red-text text-center"> Vendre un objet <br/><small class="red-text text-center">Entrez les informations sur l'objet</small> </h1>
                                <br/><br/>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="nom" class="sr-only">Nom</label>
                            <input type="text" id="nom" name="nom" class="form-control" placeholder="Nom de l'article" required autofocus>
                        </div>
                        <div class="form-group">
                            <label for="description" class="sr-only">Descrption</label>
                            <input type="text" id="description" name="description" class="form-control" placeholder="Description" required>
                        </div>
                        <select class="form-control" name="categorie" id="categorie" required>
                            <option default disabled>Categorie de l'objet</option>
                            <?php
                            while($cat= $categories->fetch()){
                                echo '<option value = "'.$cat['id_cat'].'">'.$cat['nom_cat'].'</option>';
                            }

                            ?>
                        </select>
                        <br/>
                        <div class="input-group">
                            <label for="prix_bas" class="sr-only">Prix_bas</label>
                            <input type="number" id="prix_bas" name="prix_bas" class="form-control" placeholder="Le prix de base" required>
                            <span class="input-group-addon" id="basic-addon1">€</span>
                        </div>
                        <br>
                        <div class="input-group">
                            <label for="prix_reserve" class="sr-only">Prix_reserve</label>
                            <input type="number" id="prix_reserve" name="prix_reserve" class="form-control" placeholder="Le prix de réserve" required>
                            <span class="input-group-addon" id="basic-addon1">€</span>
                        </div>
                        <br>
                        <div class="input-group col-xs-12">
                            <label for="date_limit" >Date Limite</label>
                            <div class="input-group">
                                <input type="date" id="date_limit" name="date_limit" class="form-control"  placeholder="YYYY-MM-DD" required>
                                <span class="input-group-addon" id="basic-addon1">D</span>
                            </div>

                        </div>
                        <br>
                        <div class="input-group col-xs-12">
                            <label for="heure_limit" >Heure limite</label>
                            <div class="input-group">
                                <input type="time" id="heure_limit" name="heure_limit" class="form-control" placeholder="HH:MM" required>
                                <span class="input-group-addon" id="basic-addon1">H</span>
                            </div>

                        </div>
                        <br />
                        <!--                <div class='input-group date form_date' data-date-format="yyyymmdd">-->
                        <!--                    <input name="startTm" id="startTm" type='text' class="form-control input-sm" readonly="readonly"/>-->
                        <!--                    <span class="input-group-addon input-sm">-->
                        <!--                        <span class="glyphicon glyphicon-calendar"></span>-->
                        <!--                    </span>-->
                        <!--                </div><br>-->
                        <!--                <div class="form-group">-->
                        <!--                    <label for="date_limite" class="sr-only">Prix_reserve</label>-->
                        <!--                    <input type="text" id="date_limite" name="date_limite" class="form-control" placeholder="Le date limite xx/xx/xxxx" required>-->
                        <!--                </div>-->
                        <select class="form-control" name="visibilite" id="visibilite">
                            <option value = "1" selected = "selected">Vente publique</option>
                            <option value = "2">Vente privée</option>
                            <option value = "3">Vente confidentielle</option>
                        </select>
                        <br /> <br />

                        <div class="form-group">
                            <button class="btn btn-lg btn-primary btn-block" type="submit">Ouvrir aux enchères</button>
                        </div>

                    </form>
                </div>
                <br/><br/><br/><br/><br/>
            </div>
        </div>

        <div class="container-fluid text-center" style="background-color:#C0C0C0; height:15%;">
            <br/>
            <p>M2 - Test7 &copy; 2017</p>
        </div>
    </body>
</html>
