<?php
	include_once('header.php');
?>
<html>
    <head>
        <title>Choix</title>
        <style>
            .col-center-block {
                float: none;
                display: block;
                margin-left: auto;
                margin-right: auto;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="row myCenter">
                <div class="col-xs-6 col-md-4 col-center-block">
                    <br /><br />
					<h4>Que voulez vous faire ?</h4>
					<br /><br />
                    <a href="formulaire_connexion.php"><button class="btn btn-lg btn-primary btn-block" type="submit">se connecter</button></a>
					<br />
                    <a href="formulaire_inscription.php"><button class="btn btn-lg btn-success btn-block" type="submit">s'inscrire</button></a>
					<br />
                    <a href="formulaire_enchere_visiteur.php?id_objet=<?php echo $_GET['id_objet'] ?>"><button class="btn btn-lg btn-warning btn-block" type="submit">continuer sans compte</button></a>
                </div>
            </div>
        </div>
    </body>
</html>

<?php
	include_once('footer.php');
?>
