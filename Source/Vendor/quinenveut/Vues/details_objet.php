<?php
$id_objet = $_GET['id_objet'];

require('../Objets.php');
$objets_controller = new \quinenveut\Objets();
$infos_objet = $objets_controller->get_objet($id_objet);
$objet = $infos_objet->fetch();

require('../Encheres.php');
$encheres_controller = new \quinenveut\Encheres();
$infos_encheres = $encheres_controller->get_enchere_max($id_objet);
$enchere = $infos_encheres->fetch();

require('../Categories.php');
$categories_controller = new \quinenveut\Categories();
$infos_categories = $categories_controller->get_categorie($objet['id_cat']);
$categorie = $infos_categories->fetch();

require('../Users.php');
$users_controller = new \quinenveut\Users();
$infos_vendeur = $users_controller->get_user($objet['id_vendeur']);
$vendeur = $infos_vendeur->fetch();
?>

<html>
    <head>
        <title>Profil</title>
    </head>
    <body>
        <br><br><br><br><br>
        <div class="col-md-12 col-md-offset-2 margin">
            <ul class="list-group col-sm-8" >
                <li class="list-group-item active"> Informations sur l'objet</li>
                <li class="list-group-item"><b>Objet : </b><?php echo $objet['nom_objet'] ?></li>
                <li class="list-group-item"><b>Description : </b><?php echo $objet['desc_objet'] ?></li>
                <li class="list-group-item"><b>Meilleure enchère : </b><?php echo $enchere['prix'] ?> €</li>
                <li class="list-group-item"><b>Catégorie : </b><?php echo $categorie['nom_cat'] ?></li>
                <li class="list-group-item"><b>Vendeur : </b><a href="page_profil.php?id_user=<?php echo $objet['id_vendeur']?>"><?php echo $vendeur['nom'] ?></a></li>
                <br />
            </ul>
        </div>
    </body>
</html>
