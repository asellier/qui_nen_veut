<!DOCTYPE html>
<html>
<head>
    <script>
        function del() {
            var msg = "Voulez vous supprimer cet utilisateur!";
            if (confirm(msg)==true){
                return true;
            }else{
                return false;
            }
        }
    </script>
</head>
<body>

<?php
include_once('header.php');
?>



<div class="row">
    <section class="col-sm-8 col-md-offset-1 margin">
        <table class="table table-bordered table-striped table-condensed">
            <caption>
                <h4>Liste des utilisateurs validés</h4>
            </caption>
            <thead>
            <tr>
                <th>Mail</th>
                <th>Nom</th>
                <th>Prenom</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            require_once('../Users.php');
            $users_controller = new \quinenveut\Users();
            $users = $users_controller->get_users();
            $users=$users->fetchAll();
            foreach ($users as $user) {
                if (($user['statut'] == 1) and ($user['id_role'] == 2)) {
                    echo '<tr><td>' . $user['mail'] . '</td><td> ' . $user['nom'] . '</td><td> ' . $user['prenom'] . '</td>';
                    echo '<td><a href="detail_user.php?id_user=' . $user['id_user'] . '"><button class="btn btn-primary">Details</button></a></td>';
                    echo '<td><a href="../supprimer_user.php?id_user=' . $user['id_user'] . '"><input type="hidden" name="nom" value="'.$user['id_user'].' "><input class ="btn btn-danger" type="submit " value="Supprimer"  onclick="javascript:return del();"></td></td></tr>';
                }
            }

            ?>
            </tbody>
        </table>
        <div class="row">
            <a href="page_validation_users.php"><button class="btn btn-warning">Liste des utilisateurs à valider</button></a><br><br>
            <a href="page_admin.php"><button class="btn btn-warning">Retour</button>
        </div>
        <br/>
    </section>

</div>



<?php
include_once('footer.php');
?>
</body>
</html>
