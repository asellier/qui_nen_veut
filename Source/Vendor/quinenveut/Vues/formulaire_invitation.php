<?php
include_once('header.php');
require('../Mail.php');
?>
<html>
    <head>
		<title>Inviter Un Ami</title>
        <style>
            .col-center-block {
    float: none;
    display: block;
    margin-left: auto;
                margin-right: auto;
            }
        </style>
    </head>

    <body>
    <div class="container">
        <div class="row myCenter">
            <div class="col-xs-6 col-md-4 col-center-block">
                <form class="form-signin" method="post" action="../send_invitation.php">
                    <div class="row">
                        <div class="center-block" >
                            <h3 class="red-text text-center"> Inviter Une Personne</h3>
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="email" >Á</label>
                        <input type="text" id="email" name="mail_to" class="form-control" placeholder="Enter email" required autofocus> <br>
                    </div>
                    <div class="form-group">
                        <label for="objet" >Object</label>
                        <input type="text" id="text" name="mail_sub" class="form-control" placeholder="Objet d'invitation" required><br>
                        <br>
                    </div>
                    <div class="form-group">
                        <label for="inputPassword" >Message</label>
                        <input type="text"  name="mail_msg" class="form-control" placeholder="Message ou URL de l'objet" required><br>
                        <br>
                    </div>

                    <div class="form-group">
                        <button class="btn btn-lg btn-primary btn-block" type="submit">Envoyer</button>
                    </div>

                </form>
            </div>
        </div>
    </div>
    </body>
</html>
<?php
include_once('footer.php');
?>
