<div class="row">
    <section class="col-sm-8 col-md-offset-1 margin">
        <table class="table table-bordered table-striped table-condensed">
            <caption>
                <h4>Liste des objets en attente de validation</h4>
            </caption>
            <thead>
            <tr>
                <th>Objet</th>
                <th>Vendeur</th>
                <th>Prix de départ</th>
                <th>Prix de reserve</th>
                <th>Description</th>
                <th></th>
                <th></th>
            </tr>
            </thead>
            <tbody>
            <?php
            require_once ('../Objets.php');
            $controller_objets = new \quinenveut\Objets();
            $infos_objets = $controller_objets->list_objets_a_valider();
            ?>
            </tbody>
        </table>
    </section>
</div>
<div class="row">
    <a href="page_admin_objets.php"><button class="btn btn-primary">Retour</button></a>
</div>