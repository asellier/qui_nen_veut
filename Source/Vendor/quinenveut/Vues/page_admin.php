<?php
include_once('header.php');
require_once ('../Objets.php');
$controller_objets = new \quinenveut\Objets();
$numer_objets_a_valider = $controller_objets->count_objets_a_valider();
$numer_objets_a_en_cours = $controller_objets->count_objets_en_cours();
$numer_objets_vendu = $controller_objets->count_objets_vendu();

require_once ('../Users.php');
$controller_users = new \quinenveut\Users();
$numer_users_a_valider = $controller_users->count_user_a_valider();

?>

<div class ="menu" id="menu1">
<a href="page_admin_categories.php"><h5 style="color: #1b2426">Gestion des catégories</h5></a>
</div>
<div class ="menu" id="menu2">
<a href="page_admin_objets.php"><h5 style="color: #1b2426">Gestion des enchères</h5></a>
</div>
    <div class ="menu" id="menu3">
<a href="page_admin_users.php"><h5 style="color: #1b2426">Gestion des utilisateurs</h5></a>
    </div>

        <h3 style="margin-top: 70px">Statistiques</h3>


<div class ="content1" id="row1">
    <h4>Nombre d'enchères à valider</h4>
    <h3><?php echo $numer_objets_a_valider ?></h3>
</div>
<div class ="content1" id="row2">
    <h4>Nombre d'utilisateurs à valider</h4>
    <h3><?php echo $numer_users_a_valider ?></h3>
</div>
<div class ="content1" id="row3">
    <h4>Nombre d'enchères en cours</h4>
    <h3><?php echo $numer_objets_a_en_cours ?></h3>

</div>
<div class ="content1" id="row4">
    <h4>Nombre d'enchères terminées</h4>
    <h3><?php echo $numer_objets_vendu?></h3>
</div>



<style>

    div.content1{
        background-color: #1dc116;
        height: 100px;
        border: solid;
        border-color: #0f192a;
        width: 400px;
        margin-left: 50px;
        margin-top: 30px;
        float:left;
    }

    div.menu{
        height: 40px;
        border: solid;
        width: 300px;
        margin-top: 10px;
        float:left;
        margin-left: 30px;

    }
    div#menu1{
        background-color: #96daff;
    }
    div#menu2{
        background-color: #aec4de;
    }
    div#menu3{
        background-color: silver;
    }
    div#row1{
        background-color: antiquewhite;
    }
    div#row2{
        background-color: pink;
    }
    div#row3{
        background-color: whitesmoke;
    }
    div#row4{
        background-color: #ebdb8d;
    }

</style>

<?php
include_once('footer.php');
?>
