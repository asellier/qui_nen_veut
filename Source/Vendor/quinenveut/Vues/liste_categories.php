<div class="row">
    <section class="col-sm-8 col-md-offset-1 margin">
        <table class="table table-bordered table-striped table-condensed">
            <caption>
                <h4>Liste des catégories</h4>
            </caption>
            <thead>
            <tr>
                <th>Id</th>
                <th>Description</th>
                <th>Objets en vente pour cette catégorie</th>
            </tr>
            </thead>
            <tbody>
            <?php
            require_once('../Categories.php');
            $categories_controller = new \quinenveut\Categories();
            $categories_controller->list_all_categories();
            ?>
            </tbody>
        </table>
    </section>
</div>
